# Docker Labs

Docker est un orchestrateur de conteneurs applicatifs. Kesako ?

## Docker Community Edition

### Introduction

Docker est un programme libre qui s'installe au sein d'une distribution Linux. Il permet d'exécuter des applications dans un environnement isolé du reste du système. A la différence d'un système d'exploitation virtualisé, l'environnement ne dispose pas de séquence démarrage ni de gestionnaire des tâches. Il est directement exécuté par le moteur Docker et se comporte alors comme un programme classique du système d'exploitation avec certaines limitations.

### Image Docker

Dans un environnement isolé ou image Docker, l'application dispose de l'ensemble des bibliothèques nécessaires et de la configuration requise pour son fonctionnement. Tel un programme binaire, l'image est compilé avec les sources et les dépendances de l'application puis stockée sur le système de fichier ou dans un dépôt distant.

### Conteneur Docker

La ligne de commande de Docker permet de gérer le cycle de vie des environnements et d'en manipuler leur structure : compilation, démarrage/arrêt, stockage, réseau, référencement. Lors du démarrage de l'environnement isolé, Docker créé un conteneur. Le conteneur est un processus sur le système hôte qui dispose des ressources, des variables de configuration et des paramètres d'exécution spécifiés par l'administrateur.

## Environnement

| Hôte | OS       | CPU | RAM | Description                              |
| ---- | -------- | --- | --- | ---------------------------------------- |
| labs | CentOS 7 | 2   | 4GB | Une simple machine physique ou virtuelle |

## Installation

### Composant du système

```shell
[root@labs]# yum update -y
[root@labs]# yum install -y \
    yum-utils \
    device-mapper-persistent-data \
    lvm2
```

### Dépot RPM

```shell
[root@labs]# yum-config-manager --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

Installation des paquets Docker

```shell
[root@labs]# yum install -y \
    docker-ce \
    docker-ce-cli \
    containerd.io
```

Activation et démarrage du service Docker

```shell
[root@labs]# systemctl enable docker.service
[root@labs]# systemctl start docker.service```
```

Vérifier le démarrage du service Docker

```shell
[root@labs]# systemctl status docker.service
```

La commande ***systemctl status*** doit retourner :

```
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2020-01-19 10:14:38 UTC; 24s ago
     Docs: https://docs.docker.com
 Main PID: 4070 (dockerd)
    Tasks: 13
   Memory: 44.3M
   CGroup: /system.slice/docker.service
           └─4070 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/cont...

janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.313952...c
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.314016...c
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.314073...c
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.369416..."
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.747030..."
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.835771..."
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.890949...5
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.891330..."
janv. 19 10:14:38 labs dockerd[4070]: time="2020-01-19T10:14:38.940879..."
janv. 19 10:14:38 labs systemd[1]: Started Docker Application Containe....
```

Vérification de la version du moteur Docker installé

```shell
[root@labs]# docker version
 Version:           19.03.5
 API version:       1.40
 Go version:        go1.12.12
 Git commit:        633a0ea
 Built:             Wed Nov 13 07:25:41 2019
 OS/Arch:           linux/amd64
 Experimental:      false

Server: Docker Engine - Community
 Engine:
  Version:          19.03.5
  API version:      1.40 (minimum version 1.12)
  Go version:       go1.12.12
  Git commit:       633a0ea
  Built:            Wed Nov 13 07:24:18 2019
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.2.10
  GitCommit:        b34a5c8af56e510852c35414db4c1f4fa6172339
 runc:
  Version:          1.0.0-rc8+dev
  GitCommit:        3e425f80a8c931f88e6d94a8c831b9d5aa481657
 docker-init:
  Version:          0.18.0
  GitCommit:        fec3683
```

[Voir aussi: documentation officielle](https://docs.docker.com/install/linux/docker-ce/centos/)

(optionnel) Ajout d'utilisateurs pour administrer les conteneurs Docker

```shell
[root@labs]# usermod -aG docker user1
[root@labs]# usermod -aG docker user2
...
[root@labs]# reboot
```

(optionnel) Installation d'outil permettant l'inspection des conteneurs.

```shell
[root@labs]# yum install -y \
    net-tools \
    lsof \
    tcpdump \
    nmap \
    vim \
    tree \
    curl
```

## Démarrage

### Image Docker

#### Récupérer une image

Récupération de l'image "centos:7" depuis le registre officiel

```shell
[root@labs]# docker pull centos:7
7: Pulling from library/centos
ab5ef0e58194: Pull complete 
Digest: sha256:4a701376d03f6b39b8c2a8f4a8e499441b0d567f9ab9d58e4991de4472fb813c
Status: Downloaded newer image for centos:7
docker.io/library/centos:7
```

#### Lister les images

Pour afficher la liste des images téléchargé sur le système hôte.

```shell
[root@labs]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
centos              7                   5e35e350aded        2 months ago        203MB
```

#### Supprimer une image

Suppression de l'image ***centos:7***

```shell
[root@labs]# docker rmi centos:7
```

### Conteneur Docker

#### Démarrage d'un conteneur

Dans l'étape précédente, l'image ***centos:7*** à été supprimée. Si Docker ne trouve pas de version de l'image sur le système hôte, il télécharge la meilleure correspondance depuis le registre officiel.

```shell
[root@labs]# docker run -it --name skywalker centos:7 bash
...
(conteneur)# yum -y update
(conteneur)# yum install curl
(conteneur)# curl -L https://ifconfig.me/ip
(conteneur)# exit
```

Liste des conteneurs démarrés

```shell
[root@labs]# docker ps
```

Le retour de commande n'affiche pas le conteneur centos:7 car celui à été arrêté par ***exit***.

Liste tous les conteneurs

```shell
[root@labs]# docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
105db3314b1d        centos:7            "bash"              7 minutes ago       Exited (0) 34 seconds ago                       skywalker
```

Suppression du conteneur centos:7

```shell
[root@labs]# docker rm -f skywalker
```

#### Démarrage avec le réseau

Docker permet d'accéder à l'application exécutée grâce à la redirection des flux réseaux de l'hôte vers le conteneur. Le pare-feu natif *iptables/netfilter* des distributions Linux pour effectuer le filtrage NAT.

**Avantages:**

**Inconvénients:**

```shell
[root@labs]# docker run \
    --name web \
    --detach \
    --publish 8080:80 \
    httpd
...
Unable to find image 'httpd:latest' locally
latest: Pulling from library/httpd
8ec398bc0356: Pull complete 
354e6904d655: Pull complete 
27298e4c749a: Pull complete 
10e27104ba69: Pull complete 
36412f6b2f6e: Pull complete 
Digest: sha256:769018135ba22d3a7a2b91cb89b8de711562cdf51ad6621b2b9b13e95f3798de
Status: Downloaded newer image for httpd:latest
69fed44298df0ce6f35f82d53335b4a6baab6156a386cec9fe8c85cd27dec3a0
```

| Paramètre       | Description                                                          |
| --------------- | -------------------------------------------------------------------- |
| run             | Démarre l'image téléchargée sur l'hôte avec sa commande par défaut   |
| --detach        | Exécute le conteneur en arrière plan                                 |
| --publish 80:80 | Publie le port 80 du conteneur sur le port 80 de l'hôte              |
| --name          | Attribut un nom au conteneur pour une utilisation ultérieure         |

```shell
[root@labs]# docker ps
```

### Démarrage avec un volume

```shell
[root@labs]# docker run --name web \
    --detach \
    --publish 8080:80 \
    --volume './src/:/usr/local/apache2/htdocs/' \
    httpd
...
[root@labs]# docker ps
```

## Installation de Docker Compose

```shell
$ yum update
$ yum install -y python-pip
$ pip install docker-compose
```

## Lancement d'un environnement Docker Compose

Création d'un fichier de description

```yaml
version: '3.7'

services:

    web:
        image: httpd:2.4
        ports:
            - "5050:50"
        volumes:
            - "/root/docker:/usr/local/apache2/htdocs"

volumes: {}

networks: {}
```

Dans le même répertoire, démarrer l'environnement Docker

```shell
docker-compose up -d
```

* -d ) Exécute les conteneurs en arrière-plan.

1. Modifier le fichier 'index.html'
2. Vérifier que les modifications sont bien en ligne.

## Construction d'images

Dockerfile

```text
FROM debian:stretch
LABEL maintener="Jonathan Vagnier <adm.jvagnier@gmail.com"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -y --no-install-suggests --no-install-recommends \
        python \
        python-setuptools \
        apache2 \
        libapache2-mod-wsgi \
        supervisor \
        wget \
        ca-certificates

RUN echo " *** setting up Apache Web Server ..." && \
    sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf && \
    a2enmod rewrite && \
    a2enmod wsgi

COPY src/apache2/vhost /etc/apache2/sites-available/000-default.conf
COPY src/apache2/supervisor /etc/supervisor/conf.d/apache2.conf

EXPOSE 8080

HEALTHCHECK --interval=60s --timeout=30s CMD nc -zv localhost 8080 || exit 1

CMD ["supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]
```

Editer le fichier docker-compose.yml

```yaml
version: '3.7'

services:

    web:
        image: mywebapp
        build: build/
        ports:
            - "8080:80"
        # volumes:
        #    - "/root/docker:/usr/local/apache2/htdocs"

volumes: {}

networks: {}
```

## Failover & Scalability

Arborescence du projet

```
├── build
│   ├── Dockerfile
│   └── nginx
│       ├── supervisor
│       └── vhost
├── docker-compose.yml
├── README.md
└── src
    └── index.html
```

**Editer le fichier:** ./docker-compose.yml

```yaml
version: '3.7'

services:

    web-base:
        image: mywebapp
        build: build/
        command: date

    web-1:
        image: mywebapp
        restart: always
        environment:
            SERVICE_NAME: websrv
            SERVICE_TAGS: urlprefix-/httpd
            SERVICE_CHECK_HTTP: '/'
            SERVICE_CHECK_INTERVAL: '30s'
        expose:
            - "8080"
        volumes:
            - "./src:/var/www/html"

    web-2:
        image: mywebapp
        restart: always
        environment:
            SERVICE_NAME: websrv
            SERVICE_TAGS: urlprefix-/httpd
            SERVICE_CHECK_HTTP: '/'
            SERVICE_CHECK_INTERVAL: '30s'
        expose:
            - "8080"
        volumes:
            - "./src:/var/www/html"

    fabio-1:
        image: fabiolb/fabio
        restart: always
        environment:
            SERVICE_9998_NAME: fabio
            SERVICE_9998_TAGS: urlprefix-fabio.${DOMAIN}/
            SERVICE_9998_CHECK_HTTP: '/routes'
            SERVICE_9998_CHECK_INTERVAL: '30s'
        ports:
            - '80:9999'
            - '9998:9998'
        command: -registry.consul.addr 'consul-1:8500'
        depends_on:
            - consul-1

    registrator-1: &consul-registrator
        image: gliderlabs/registrator:latest
        restart: always
        network_mode: host
        volumes:
            - /var/run/docker.sock:/tmp/docker.sock
        command: |
            -cleanup
            -internal
            consul://127.0.0.1:8500

    consul-1:
        image: consul:latest
        restart: always
        # network_mode: host
        environment:
            SERVICE_8500_NAME: consul-ui
            SERVICE_8500_TAGS: 'urlprefix-consul.${DOMAIN}/'
            SERVICE_8500_CHECK_HTTP: '/ui'
            SERVICE_8500_CHECK_INTERVAL: 30s
            SERVICE_8500_CHECK_TIMEOUT: 10s
        ports:
            - '8400:8400'
            - '8500:8500'
            - '127.0.0.1:53:8600'
            - '127.0.0.1:53:8600/udp'
        command: 'agent -ui -server -bootstrap-expect 1 -advertise 127.0.0.1 -client 0.0.0.0'

volumes: {}

networks: {}
```

**Editer le fichier:** ./build/Dockerfile

```dockerfile
FROM debian:stretch
LABEL maintener="Jonathan Vagnier <adm.jvagnier@gmail.com"

# Install NGinx Web Server
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -y --no-install-suggests --no-install-recommends \
        python \
        python-setuptools \
        nginx \
        supervisor \
        wget \
        ca-certificates

COPY nginx/vhost /etc/nginx/sites-available/default
COPY nginx/supervisor /etc/supervisor/conf.d/nginx.conf

EXPOSE 8080

HEALTHCHECK --interval=60s --timeout=30s CMD nc -zv localhost 8080 || exit 1

CMD ["supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]
```

Construction de l'image

```shell
docker-compose build web-base
```

Démarrage des services

```shell
docker-compose up -d
```

Vérification de l'état des services

```shell
docker-compose ps
docker-compose logs web-1 web-2
...
```